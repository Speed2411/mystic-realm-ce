                   ###########################################
                  ###########################################
                 #### "MYSTIC REALM: COMMUNITY EDITION" ####
                ####          "DEVELOPMENT INFO"       ####
               ###########################################
              ###########################################

 #########################################################################
###########################################################################
###                                                                     ###
### "THANK YOU FOR CONTRIBUTING TO PLAYTESTING AND"                     ###
### "DEVELOPMENT OF THE MYSTIC REALM: COMMUNITY EDITION"                ###
###                                                                     ###
### ALL MYSTIC REALM: COMMUNITY EDITION ASSETS MAY BE REUSED            ###
### "IN FACT, WE ENCOURAGE YOU TO!"                                     ###
###                                                                     ###
### ANY PERSON MAY MAKE ANY CONTRIBUTION, BIG OR SMALL,                 ### 
### VIA EITHER OUR OFFICIAL DISCORD OR OUR GITLAB.                      ###
###                                                                     ###
### JOIN THE TEAM PRISMATIC DEVELOPMENT DISCORD                         ###
### "https://discord.gg/WcB2vaqbgf"                                     ###
###                                                                     ###
### CONTRIBUTE TO THE MYSTIC REALM CE GITLAB                            ###
### "https://gitlab.com/team-prismatic/mystic-realm-ce"                 ###
###                                                                     ###
### LEAVE A COMMENT ON THE TEAM PRISMATIC MESSAGE BOARD PAGE            ###
### "https://mb.srb2.org/threads/29931/"                                ###
###                                                                     ###
### THANK YOU                                                           ###
###                                                                     ###
###########################################################################
 #########################################################################