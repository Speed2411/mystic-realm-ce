addHook("MobjThinker", function(mo)
	if not mo.target then P_KillMobj(mo) return end

	--print(mo.target.player.powers[pw_shield])

	if mo.target.player.powers[pw_shield] == SH_GRAVITY then
		if not (mo.flags2 & MF2_DONTDRAW) then
			if mo.target.flags2 & MF2_OBJECTFLIP then
				mo.color = SKINCOLOR_SUPERORANGE5
				A_Sparkling(mo,MT_GRAVITORB_PART_O)
			else
				mo.color = SKINCOLOR_CORNFLOWER
				A_Sparkling(mo,MT_GRAVITORB_PART_B)
			end
		end
	elseif mo.target.player.powers[pw_shield] == SH_CLOCK then
		mo.target.player.nuhuh = $ or false
		mo.color = SKINCOLOR_JET
		if not mo.clocklay then
			mo.clocklay = P_SpawnMobj(mo.x,mo.y,mo.z,MT_OVERLAY)
			mo.clocklay.target = mo
			mo.clocklay.state = S_CLOCK_OVERLAY
			mo.clockhr = P_SpawnMobj(mo.x,mo.y,mo.z,MT_OVERLAY)
			mo.clockhr.target = mo.clocklay
			mo.clockhr.state = S_CLOCK_OVERLAY_HR
			mo.clockmin = P_SpawnMobj(mo.x,mo.y,mo.z,MT_OVERLAY)
			mo.clockmin.target = mo.clocklay
			mo.clockmin.state = S_CLOCK_OVERLAY_MIN
		end
		if mo.target.player.pflags & PF_JUMPED and not (mo.target.player.pflags & PF_THOKKED) then
			mo.clockmin.rollangle = $-ANG1
			mo.clockhr.rollangle = $-ANG1/8
		end
		if mo.target.player.pflags & PF_SHIELDABILITY then
			mo.clockmin.rollangle = 0
			mo.clockhr.rollangle = 0
			mo.target.player.pflags = $ & ~PF_SHIELDABILITY
		end
		if not (mo.flags2 & MF2_DONTDRAW) then
			mo.clocklay.flags2 = $ & ~MF2_DONTDRAW
			mo.clockhr.flags2 = $ & ~MF2_DONTDRAW
			mo.clockmin.flags2 = $ & ~MF2_DONTDRAW
		else
			mo.clocklay.flags2 = $ | MF2_DONTDRAW
			mo.clockhr.flags2 = $ | MF2_DONTDRAW
			mo.clockmin.flags2 = $ | MF2_DONTDRAW
		end
		if mo.target.player.pflags & PF_STARTJUMP and not mo.clockrw then
			if mo.target.player.mrce.realspeed == 0 then
				mo.clockrw = P_SpawnMobjFromMobj(mo.target,0,0,0,MT_CLOCK_REWIND)
			else
				mo.clockrw = P_SpawnMobj(mo.target.x - FixedMul(mo.target.player.mrce.realspeed, cos(mo.target.angle)),
											mo.target.y - FixedMul(mo.target.player.mrce.realspeed, sin(mo.target.angle)),
											mo.target.z, MT_CLOCK_REWIND)
			end
			mo.clockrw.target = mo.target
			mo.clockrw.tracer = mo
			mo.clockrw.angle = mo.target.angle
			mo.target.rewindpoint = mo.clockrw
		end
	end
	if mo.target.player.powers[pw_shield] != SH_CLOCK and mo.clocklay then
		P_KillMobj(mo.clocklay)
		mo.clocklay = nil
		mo.target.clockx = nil
	end
	mo.scale = skins[mo.target.skin].shieldscale + (FRACUNIT/10)
	if (mo.target.player.powers[pw_invulnerability] > 0)
	or (mo.target.player.powers[pw_super] > 0) then
		mo.flags2 = $ | MF2_DONTDRAW
	else
		mo.flags2 = $ & ~MF2_DONTDRAW
	end
	if mo.target.player.powers[pw_shield] != SH_GRAVITY
	and mo.target.player.powers[pw_shield] != SH_CLOCK then
		mo.target.flags2 = $ & ~MF2_OBJECTFLIP
		mo.target.star_timer = 0
		P_KillMobj(mo)
		return
	end
	if (mo.target.sprite2 == SPR2_ROLL)
	or (mo.target.sprite2 == SPR2_SPIN)
	or (mo.target.sprite2 == SPR2_GLID) then
		A_CapeChase(mo,(-7*FRACUNIT)+0)
	else
		A_CapeChase(mo)
	end
end, MT_EXTRA_ORB)

addHook("PlayerThink", function(p)
	if p.playerstate ~= PST_LIVE or not (p.mo and p.mo.valid) then return end
	if ((p.powers[pw_shield] & SH_NOSTACK) ~= SH_CLOCK) then return end
	if ((p.pflags & PF_STARTJUMP) or (P_IsObjectOnGround(p.mo) and (p.mo.state == S_PLAY_SKID or p.mo.state == S_PLAY_WALK or p.mo.state == S_PLAY_RUN or p.mo.state == S_PLAY_ROLL or p.mo.state == S_PLAY_EDGE or p.mo.state == S_PLAY_GLIDE_LANDING or p.mo.state == S_PLAY_STND or p.mo.state == S_PLAY_WAIT))) and not p.backupplanrewind then
		p.backupplanrewind = {p.mo.x, p.mo.y, p.mo.floorz, p.mo.angle, 1}
	end
	if P_IsObjectOnGround(p.mo) and p.backupplanrewind ~= nil then
		if p.backupplanrewind[5] > 0 then
			p.backupplanrewind[5] = $ - 1
		else
			p.backupplanrewind = nil
		end
	end
end)

local function doRewind(player, x, y, z, a)
	if player.mo.rewindpoint or x ~= nil then
		S_StartSound(player.mo, sfx_kc31)
		P_FlashPal(player,PAL_WHITE,10)
		local bx = x or player.mo.rewindpoint.x
		local by = y or player.mo.rewindpoint.y
		local bz = z or player.mo.rewindpoint.z
		local ba = a or player.mo.rewindpoint.angle
		--print(tostring(bx/FU) .. " " .. tostring(by/FU) .. " " .. tostring(bz/FU))
		P_SetOrigin(player.mo, bx, by, bz)
		A_ForceStop(player.mo)
		player.mo.angle = ba
		P_ResetPlayer(player)
		player.powers[pw_nocontrol] = 17
		--player.pflags = $ | PF_THOKKED | PF_SHIELDABILITY
	end
end

addHook("ShieldSpecial", function(player)
	if player.powers[pw_shield] == SH_GRAVITY then
		if player.mo.flags2 & MF2_OBJECTFLIP then
			player.mo.flags2 = $ & ~MF2_OBJECTFLIP
		else
			player.mo.flags2 = $ | MF2_OBJECTFLIP
		end
		S_StartSound(player.mo, sfx_peww)
		player.pflags = $ | PF_THOKKED
		return true
	elseif player.powers[pw_shield] == SH_CLOCK then
		doRewind(player)
		return true
	end
end)

addHook("ShouldDamage", function(mo, inf, source, dmg, dmgtype)
		if dmgtype == DMG_DEATHPIT and (mo.player.powers[pw_shield] == SH_CLOCK) and mo.player.backupplanrewind then
		local x, y, z, a = mo.player.backupplanrewind[1], mo.player.backupplanrewind[2], mo.player.backupplanrewind[3], mo.player.backupplanrewind[4]
		doRewind(mo.player, x, y, z, a)
		local extraorb = P_SpawnMobj(mo.x,mo.y,mo.z,MT_EXTRA_ORB)
		extraorb.target = mo
		return false
	end
end, MT_PLAYER)