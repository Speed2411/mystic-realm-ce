--thanks spec for this lua, youre amazing

freeslot("MT_ICEBEAM", "MT_ICEBEAMSHARD", "MT_ICEBEAMPARTICLE", "S_ICEBEAMPARTICLE", "MT_ICECHILLAPAIN")

mobjinfo[MT_ICECHILLAPAIN] = {
	spawnstate = S_INVISIBLE,
	spawnhealth = 1,
	reactiontime = 8,
	deathstate = S_XPLD1,
	deathsound = sfx_pop,
	radius = 32*FRACUNIT,
	height = 32*FRACUNIT,
	mass = 100,
	flags = MF_NOGRAVITY|MF_NOCLIP|MF_NOCLIPHEIGHT|MF_SPECIAL
}


-- Just don't, it is waste.
--local debug = 0

local function onfire(player)
	if player.spectator then return false end
	if not player.realmo then return false end
	if (player.powers[pw_shield] & SH_FLAMEAURA)
	or (player.mo.skin == "supersonic" and (player.powers[pw_shield] & SH_FIREFLOWER))
	or (player.mo.skin == "blaze" and (player.solchar and player.solchar.istransformed))
	or (player.mo.skin == "blaze" and player.blazeboosting and not (player.mo.eflags & MFE_UNDERWATER))
	or (player.mo.skin == "blaze" and player.mo.blazejumping and not (player.mo.eflags & MFE_UNDERWATER))
	or (player.mo.skin == "blaze" and player.blazehover and not (player.mo.eflags & MFE_UNDERWATER))
	or MRCE_isHyper(player) then
		return true
	end
	return false
end

local function IceTouchesSomething(special, toucher)
	if special and special.valid and toucher and toucher.valid and toucher.player and toucher.player.frozen ~= 1 and not toucher.player.powers[pw_flashing] and not onfire(toucher.player) then
		toucher.player.oldcolor = toucher.color
		if toucher.player.mo.state ~= nil then
			toucher.player.frozenstate = toucher.player.mo.state
		else
			toucher.player.frozenstate = 13
		end
		toucher.player.frozenframe = toucher.player.mo.frame
		toucher.player.frozenanim = toucher.player.panim
		toucher.player.frozen = 1
		toucher.player.frozentimer = 140
	end
end

addHook("TouchSpecial", IceTouchesSomething, MT_ICEBEAM)
addHook("TouchSpecial", IceTouchesSomething, MT_ICEBEAMSHARD)

addHook("PlayerThink", function(p)
	if not p.realmo then return end
	if p.spectator then return end
	--print(tostring(onfire(p)))
	p.frozen = $ or 0
	p.frozentimer = $ or 0
	p.oldcolor = $ or 0
	if onfire(p) then
		p.frozentimer = 0
		p.frozen = 0
		return
	end
	--if debug == 1 then
		--print("frozentimer: " .. p.frozentimer)
		--print("jump: " .. p.mrce.jump)
		--if (p.pflags & PF_FULLSTASIS) then
		--	print("stasis")
		--end
	--end
	if p.frozentimer == 140 then
		S_StartSound(p.mo, sfx_s3k80)
	end
	if p.frozentimer == 1 then
		S_StartSound(p.mo, sfx_shattr)
	end
	if p.frozen == 1 and p.frozentimer > 0 then
		p.mo.colorized = true
		p.mo.color = SKINCOLOR_ICY
		if p.frozenstate ~= nil then
			p.mo.state = p.frozenstate
		else
			p.frozenstate = p.mo.state
		end
		if p.frozenframe ~= nil then
			p.mo.frame = p.frozenframe
		else
			p.frozenframe = p.mo.frame
		end
		if p.frozenanim ~= nil then
			p.panim = p.frozenanim
		else
			p.frozenanim = p.panim
		end
		if p.powers[pw_flashing] then
			p.frozentimer = 0
			p.frozen = 0
			return
		end
		p.frozentimer = $-1
		p.pflags = $1|PF_FULLSTASIS
		if p.mrce and p.mrce.jump == 1 and p.frozentimer > 20 then
			p.frozentimer = $-15
			S_StartSound(p.mo, sfx_s3k80)
		end
		if not (leveltime % 50) then
			P_SpawnMobjFromMobj(p.mo, (P_RandomRange(-5, 5) * FRACUNIT), (P_RandomRange(-5, 5) * FRACUNIT), ((p.mo.height / 2) + ((P_RandomRange(-3, 3) * FRACUNIT))), MT_ICEBEAMPARTICLE)
		end
		if P_IsObjectOnGround(p.mo) and p.speed == 0 then
			p.mo.flags = $1|MF_NOTHINK
		end
		if p.followmobj and p.followmobj.type == MT_TAILSOVERLAY then
			p.followmobj.flags = $1|MF_NOTHINK
		end
	elseif p.frozen == 1 then
		p.mo.colorized = false
		p.mo.color = p.oldcolor
		p.oldcolor = 0
		p.pflags = $1 & ~PF_FULLSTASIS
		p.mo.flags = $1 & ~MF_NOTHINK
		p.frozenstate = nil
		p.frozenframe = nil
		p.frozenanim = nil
		if p.followmobj and p.followmobj.type == MT_TAILSOVERLAY then
			p.followmobj.flags = $1 & ~MF_NOTHINK
		end
		p.mo.state = S_PLAY_FALL
		p.frozen = 0
		p.frozentimer = 0
		p.powers[pw_flashing] = 40
	end
end)

addHook("PostThinkFrame", function()
    for player in players.iterate do
		if player.frozen == 1 and player.frozentimer > 0 then
			if player.frozenstate ~= nil then
				player.mo.state = player.frozenstate
			end
			if player.frozenframe ~= nil then
				player.mo.frame = player.frozenframe
			end
			if player.frozenanim ~= nil then
				player.panim = player.frozenanim
			end
		end
	end
end)

addHook("MobjDeath", function(mo)
	if mo.player and mo.player.oldcolor then
		local p = mo.player
		p.frozen = 0
		p.frozentimer = 0
		mo.color = p.oldcolor
		if p.mrce and not p.mrce.glowaura then
			p.mo.blendmode = 0
		end
		p.pflags = $1 & ~PF_FULLSTASIS
		--mo.flags = $1 & ~MF_NOTHINK
	end
end, MT_PLAYER)

local function M_ReachDestination(curr_val, dest_val, step)
    local final_val = curr_val
	if final_val < dest_val then
        final_val = $ + step
        if final_val+step > dest_val then
            final_val = dest_val
        end
    elseif final_val > dest_val then
        final_val = $ - step
        if final_val-step < dest_val then
            final_val = dest_val
        end
    end
    return final_val
end

local function P_CalcDiff(angle, another_angle)
	local dif = another_angle - angle

	if dif > ANGLE_180 and dif <= ANGLE_MAX then
		dif = $ - ANGLE_MAX
	end

	return dif
end

local function P_PathingFixedRotate(angle, next_angle, step)
    return angle + M_ReachDestination(0, P_CalcDiff(angle, next_angle), step)
end

addHook("MobjThinker", function(a)
	if a.state >= S_CRYOCRAWLA_RUN1 and a.state <= S_CRYOCRAWLA_RUN7 and not a.cusval then
		if not a.target then
			a.state = a.info.spawnstate
			return
		end

		local dist_plr = R_PointToDist2(a.x, a.y, a.target.x, a.target.y)
		local angle_plr = R_PointToAngle2(a.x, a.y, a.target.x, a.target.y)

		local angle_dif = FixedMul(AngleFixed(P_CalcDiff(a.angle, angle_plr))/180, 5*a.scale)
		P_InstaThrust(a, a.angle, 4*a.scale + angle_dif)

		-- Ice impreciseness
		if abs(a.momx) > 6*a.scale then
			a.extravalue1 = a.momx/6
		else
			a.extravalue1 = ease.linear(FRACUNIT/8, a.extravalue1, 0)
		end

		if abs(a.momy) > 6*a.scale then
			a.extravalue2 = a.momy/6
		else
			a.extravalue2 = ease.linear(FRACUNIT/8, a.extravalue2, 0)
		end

		a.momx = $+a.extravalue1
		a.momy = $+a.extravalue2

		a.angle = P_PathingFixedRotate(a.angle, angle_plr, ANG20)
		a.flags = $|MF_BOUNCE

		-- Thinker lock
		if dist_plr < 16*a.scale then
			a.cusval = 2*TICRATE
		--	a.state = S_CRYOCRAWLA_ATK1
		elseif dist_plr > 1000*FRACUNIT then
			a.target = nil
			a.state = a.info.spawnstate
			return
		end

		if not (leveltime % 8) then
			local width = (a.info.radius >> FRACBITS) * 4
			local height = (a.info.height >> FRACBITS) * 4

			local x = P_RandomRange(-width, width) * FRACUNIT
			local y = P_RandomRange(-width, width) * FRACUNIT
			local z = P_RandomRange(-height, height) * FRACUNIT

			local sheer_cold = P_SpawnMobjFromMobj(a, x, y, z, MT_ICECHILLAPAIN)
			sheer_cold.fuse = TICRATE*3
			sheer_cold.extravalue1 = sheer_cold.fuse
			sheer_cold.scale = 4*a.scale
			sheer_cold.target = a
			sheer_cold.sprite = SPR_CPOS
			sheer_cold.frame = E|FF_TRANS60
		end
	end

	if a.cusval then
		a.cusval = $-1
		if not (a.momx or a.momy) then
			a.cusval = 0
		end
	end
end, MT_CRYOCRAWLA)

addHook("MobjThinker", function(a)
	if a.extravalue1 and a.fuse then
		local transparency = ease.linear(a.fuse * FRACUNIT / a.extravalue1, 9, 8)
		a.frame = E|(transparency << FF_TRANSSHIFT)
		a.scale = $+(FRACUNIT >> 5)
	end
	a.momz = $+a.scale >> 6
end, MT_ICECHILLAPAIN)

addHook("TouchSpecial", function(a, t)
	if t.player then
		if not t.player.frozentimer then
			if a.target then
				if not t.player.mrce.freezeeffect then
					t.player.mrce.freezeeffect = 0
				end
				if not onfire(t.player) then
					t.player.mrce.freezeeffect = $+12
				end
			else
				if not t.player.mrce.freezeeffect then
					t.player.mrce.freezeeffect = 0
				end
				if not onfire(t.player) then
					t.player.mrce.freezeeffect = $+6
				end
			end

			if t.player.mrce.freezeeffect and t.player.mrce.freezeeffect > 30*TICRATE then
				IceTouchesSomething(a, t)
			end
		end
	end
	return true
end, MT_ICECHILLAPAIN)

function A_CRYOCRAWLAICESKATE(a, var1)
	A_FaceTarget(a)
	P_InstaThrust(a, a.angle, var1*a.scale)
	a.friction = $ + 4*FRACUNIT
	a.flags = $|MF_BOUNCE
end

function A_CRYOCRAWLAICERESET(a, var1)
	A_FaceTarget(a)
	a.friction = $ - 4*FRACUNIT
	a.flags = $ &~ MF_BOUNCE
end

-- Radicalicious 01/05/2023:
-- Like what is done for icicles, we're going to make sure Cryocrawlas can't
-- fire anything when the player isn't in reasonable firing distance. This
-- could be done by changing the SOC to use A_Look with distance checks like
-- how Coconuts does it, but I'm lazy and don't feel like touching that ancient
-- SOC we might replace eventually anyways.
addHook("MobjThinker", function(mo)
	if (searchBlockmap("objects", function(amo, pmo)
		if not pmo.valid then return nil end
		if pmo.player then return true else return nil end
	end, mo, mo.x - 728 << FRACBITS, mo.x + 728 << FRACBITS, mo.y - 728 << FRACBITS, mo.y + 728 << FRACBITS)) then
		if mo.state >= S_CRYOCRAWLA_RUN7 then mo.state = S_CRYOCRAWLA_RUN1 end
	end
end, MT_CRYOCRAWLA)