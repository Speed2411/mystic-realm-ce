--can i? :pleading_face:
--~xelork

freeslot("MT_MFZWINDSPAWNER", "MT_MFZWINDSNOW")

freeslot("S_MFZWINDSNOW")

sfxinfo[sfx_s3kcel].flags = $|SF_NOMULTIPLESOUND
local sfx_s3kcel = sfx_s3kcel

--[[
	Skydusk:

	This is horrible. I am sorry, but way how it searches everything just makes no sense.
	It is already horrifying for the fact it uses "ThinkFrame".
	With UDMF, all line exe stuff could be moved to spawner itself.
	(which to be fair, binary does require some hack solution. However, that shouldn't be concern anymore)
	This is very unoptimize approach, refactor is necessary and I don't care if you going to hide behind (GL)Hardware Renderer.

	Design proposition:
	(UDMF) -> player-based activation by being in tagged area (simple if to table in value trackable would have been enough) ->
	-> Player activates the object -> spawner spawns particles -> X <finished routine>
	-> All wind movement will be done in player thinkers and special mobjs for this occassion.

	Current change:
	All mobj searches skip looping until they find mobj with activate player userdata.
]]

mobjinfo[MT_MFZWINDSPAWNER] = {
	--$Category "Mystic Realm - Midnight Freeze Zone"
	--$Name Wind Particle Generator Start
	--$Sprite SNO1A0
	--$AngleText Angle/Tag
	doomednum = 36,
	spawnstate = S_INVISIBLE,
	flags = MF_NOGRAVITY|MF_NOCLIP|MF_NOBLOCKMAP|MF_NOCLIPHEIGHT|MF_SCENERY,
}

mobjinfo[MT_MFZWINDSNOW] = {
	spawnstate = S_MFZWINDSNOW,
	flags = MF_NOGRAVITY|MF_NOCLIP|MF_NOBLOCKMAP|MF_NOCLIPHEIGHT|MF_SCENERY,
}

states[S_MFZWINDSNOW] = {
	sprite = SPR_SNO1,
	frame = A,
	tics = -1,
	nextstate = S_NULL,
}

-- Optimalization measures, perhaps?
-- Sounds dumb but according to https:--www.lua.org/gems/sample.pdf paper
-- It may be even more efficient, we will see
-- Future Skydusk: Doesn't seem to do much.

-- Reducing Global/C api calls is the way.

local MFE_VERTICALFLIP = MFE_VERTICALFLIP
local PF_ANALOGMODE = PF_ANALOGMODE
local SH_WHIRLWIND = SH_WHIRLWIND
local MF_PUSHABLE = MF_PUSHABLE
local CR_ROPEHANG = CR_ROPEHANG
local ML_MIDSOLID = ML_MIDSOLID
local MFE_PUSHED = MFE_PUSHED
local MF_NOTHINK = MF_NOTHINK
local ML_EFFECT6 = ML_EFFECT6
local PF_SLIDING = PF_SLIDING

local FRACUNIT = FRACUNIT
local FRACBITS = FRACBITS
local ANG1 = ANG1

local P_SpawnMobjFromMobj = P_SpawnMobjFromMobj
local P_SpawnMobj = P_SpawnMobj

local R_PointToDist2 = R_PointToDist2
local R_PointToAngle2 = R_PointToAngle2
local P_SetOrigin = P_SetOrigin

local P_MovePlayer = P_MovePlayer
local P_ResetPlayer = P_ResetPlayer

local S_StartSound = S_StartSound
local S_SoundPlaying = S_SoundPlaying
local S_StopSoundByID = S_StopSoundByID

local FixedMul = FixedMul
local FixedDiv = FixedDiv
local FixedAngle = FixedAngle

local P_LinedefExecute = P_LinedefExecute
local P_AproxDistance = P_AproxDistance
local P_RandomRange = P_RandomRange

local cos = cos
local sin = sin
local abs = abs
local max = max
local min = min

local sectors_tagged = sectors.tagged

-- I would think even Lua functions should be localized :P
local MRCE_isHyper = MRCE_isHyper



--[[
ok so this is how the data works:

object angle = object tag (every 360 degrees is a new tag) and particles direction
linedef tag must be the same as object tag for them to work together
and linedef action must be 6, to prevent conflict

control sector height = wind max distance in fracunits
linedef length = wind speed in fracunits
control sector special = wind spawn intermission in tics
control sector brightness = number of particles to spawn at once

linedef frontside x = second obj x in fracunits
linedef frontside y = second obj y in fracunits
control sector floor height = second obj z

the two objects will connect and create an "invisible linedef" that will generate the particles
the particles will fly, following the object angle, until they reach their max distance, specified by control sector height

YOU MUST CREATE AN INVISIBLE FOF AROUND THE SNOWING AREA WITH A LINEDEF EXECUTOR TO CALL THE LUA FUNCTION "SNOSFX" FOR THE SOUND!!!!!!!!!!!!!!!!!

thats it i guess
]]

--[[
	Additional, incomplete optimizations
	The PostThinkFrame hook spawns snow particles directly around the consoleplayer
	This would help combat the weakness of the second MobjFuse hook,
	which looks for players before spawning anything. It works well for small areas,
	but not for larger ones


addHook("PostThinkFrame", function()
	if leveltime%2 and consoleplayer and consoleplayer.realmo and consoleplayer.realmo
	and consoleplayer.realmo.snowing and consoleplayer.realmo.snowing > 0 then
		local rx = P_RandomRange(consoleplayer.realmo.x/FRACUNIT - 128, (consoleplayer.realmo.x/FRACUNIT + 128))*FRACUNIT
		local ry = P_RandomRange(consoleplayer.realmo.y/FRACUNIT - 128, (consoleplayer.realmo.y/FRACUNIT + 128))*FRACUNIT
		local rz = P_RandomRange(consoleplayer.realmo.z/FRACUNIT - 128, (consoleplayer.realmo.z/FRACUNIT + 128))*FRACUNIT

		local wind = P_SpawnMobj(rx, ry, rz, MT_MFZWINDSNOW)
		local windspeed = P_RandomRange(12, 16)
		wind.frame = FF_FULLBRIGHT|P_RandomRange(0, 2)
		wind.momx = FixedMul(cos(consoleplayer.realmo.angle+ANGLE_90),(windspeed*FRACUNIT))
		wind.momy = FixedMul(sin(consoleplayer.realmo.angle),(windspeed*FRACUNIT))
		wind.tics = 256/windspeed
	end
end)

addHook("MobjFuse", function(mo)
	if not (searchBlockmap("objects", function(amo, pmo)
		if not pmo.valid then return nil end
		if pmo.player then return true else return nil end
	end, mo, mo.x-1024*FRACUNIT, mo.x+1024*FRACUNIT, mo.y-1024*FRACUNIT, mo.y+1024*FRACUNIT)) then
		for i = 1, mo.windamount do
			local rx = P_RandomRange(mo.x/FRACUNIT, mo.target.x/FRACUNIT)*FRACUNIT
			local ry = P_RandomRange(mo.y/FRACUNIT, mo.target.y/FRACUNIT)*FRACUNIT
			local rz = P_RandomRange(mo.z/FRACUNIT, mo.target.z/FRACUNIT)*FRACUNIT
			if not (R_PointToAngle2(mo.x, mo.y, rx, ry) < mo.targetangle)
				local wind = P_SpawnMobj(rx, ry, rz, MT_MFZWINDSNOW)
				local windspeed = P_RandomRange(mo.windspeed-8, mo.windspeed)
				wind.frame = FF_FULLBRIGHT|P_RandomRange(0, 2)
				wind.momx = FixedMul(cos(mo.angle),(windspeed*FRACUNIT))
				wind.momy = FixedMul(sin(mo.angle),(windspeed*FRACUNIT))
				wind.tics = mo.windmaxlen/windspeed
			end
		end
	end
	mo.fuse = mo.timer
	return true
end, MT_MFZWINDSPAWNER)
]]

local JUMPED_FLAGS = PF_JUMPED|PF_NOJUMPDAMAGE
local AVOID_FLAGS = MF_NOGRAVITY|MF_NOCLIP
local AVOID_PUSH = {
	[MT_PLAYER] = true,
	[MT_SMALLBUBBLE] = true,
	[MT_MEDIUMBUBBLE] = true,
	[MT_EXTRALARGEBUBBLE] = true,
	[MT_LITTLETUMBLEWEED] = true,
	[MT_BIGTUMBLEWEED] = true,
}

local AVOID_GRAVITY = {
	[MT_SMALLBUBBLE] = true,
	[MT_MEDIUMBUBBLE] = true,
	[MT_EXTRALARGEBUBBLE] = true,
}

-------------------------------------
-- Linedef type 541 Recoded - Barrels O' Fun
-------------------------------------
local function MFZWind(l, mo, sfx)
	local mo = mo
	local player = mo.player
	local l = l

	if sfx and player then
		S_StartSound(player.realmo, sfx_s3kcel, player)
		player.realmo.snowing = 17
	end

	local strength = R_PointToDist2(l.v2.x, l.v2.y, l.v1.x, l.v1.y) or 8 << FRACBITS
	local length = strength

	if (l.flags & ML_EFFECT6) then
		strength = sides[l.sidenum[0]].textureoffset
	end

	local hspeed = strength
	local dx = FixedMul(FixedDiv(l.dx, length), hspeed)
	local dy = FixedMul(FixedDiv(l.dy, length), hspeed)

-------------------------------------
--P_ConvertBinaryLinedefTypes section
-------------------------------------

	local args4 = 0
	if (l.flags & ML_MIDSOLID) then
		args4 = $ | 1
	end

	if (!(l.flags & ML_NOCLIMB)) then
		args4 = $ | 2
	end

-------------------------------------
-- Add_Pusher / T_Pusher section
-------------------------------------

	if (mo.flags & AVOID_FLAGS and not AVOID_GRAVITY[mo.type])
	or ((not ((mo.flags & MF_PUSHABLE)
	or ((mo.info.flags & MF_PUSHABLE) and mo.fuse)))
	and not AVOID_PUSH[mo.type])
	or (mo.eflags & MFE_PUSHED) then
		return -- Only no fuse Pushables and listed objects get pushed
	end

	if player then
		local powers = player.powers
		if (powers[pw_carry] == CR_ROPEHANG) or (powers[pw_shield] == SH_WHIRLWIND) or (MRCE_isHyper(player)) then
			return -- Players are immune if hanging on or carrying windshield
		end

		if (mo.state == states[mo.info.painstate]) and (powers[pw_flashing] > (flashingtics/4)*3 and powers[pw_flashing] <= flashingtics) then
			return -- Recently hurt players don't get pushed
		end
	end

	local xspeed = dx/128
	local yspeed = dy/128
	local windb = l.frontside.sector.floorheight
	local windt = l.frontside.sector.ceilingheight
	local check_moz = mo.z + (mo.height >> 1)

	if (check_moz > windt and !mo.eflags & MFE_VERTICALFLIP) -- Wind gets halved when mobj is halfway past the top (or bottom when flipped) of rover
	or (check_moz > windb and mo.eflags & MFE_VERTICALFLIP) then
		xspeed = xspeed/2
		yspeed = yspeed/2
	end

	mo.momx = $ + xspeed
	mo.momy = $ + yspeed

	if (player) then -- Player cmom values

		mo.player.cmomx = FixedMul(player.cmomx + xspeed, 59392)
		mo.player.cmomy = FixedMul(player.cmomy + yspeed, 59392)

		if (args4 & 1) then -- If Sliding is happening
			local jumped = player.pflags & JUMPED_FLAGS
			P_ResetPlayer(mo.player)

			if jumped then
				player.pflags = $ | JUMPED_FLAGS
			else
				mo.state = mo.info.painstate -- Doesn't get set regularly for some reason so do it here.
			end

			mo.player.pflags = $ | PF_SLIDING
			P_MovePlayer(mo.player)
			mo.angle = R_PointToAngle2(0, 0, xspeed, yspeed);

			if (player.pflags & PF_ANALOGMODE) then
				local angle = player.cmd.angleturn << 16
				if (mo.angle - angle > ANGLE_180) then
					mo.angle = (angle - (angle - mo.angle) / 8)
				else
					mo.angle = (angle + (mo.angle - angle) / 8)
				end
			end
		end
	end

	if (mo.type == MT_LITTLETUMBLEWEED or mo.type == MT_BIGTUMBLEWEED) then
		mo.momz = $ + P_AproxDistance(xspeed, yspeed) / 4
	end


	if not (args4 & 2) then
		mo.eflags = $ | MFE_PUSHED
	end
end

-------------------------------------
--Table unload/sync
-------------------------------------
local wind_rover_list = {}
local wind_lines_list = {}

addHook("MapLoad", function()
	wind_rover_list = {}
	wind_lines_list = {}

	for line in lines.iterate do
		if line.special ~= 443 then continue end

		-- not sure if it will work for binary, though game already convers
		-- specials in realtime
		--local founds = 0
		if line.stringargs[0] == 'SNOSFX' then
			for rover in sectors_tagged(line.tag) do
				wind_rover_list[#rover] = rover
				wind_lines_list[#rover] = line
				--founds = $+1
			end
		end
		--print(founds)
	end
end)

addHook("NetVars", function(network)
	wind_rover_list = network(wind_rover_list)
	wind_lines_list = network(wind_lines_list)
end)

-------------------------------------
--Linedef Execute / ThinkFrame SNOSFX
-------------------------------------

--[[
addHook("ThinkFrame", function()
	if wind_rover_lists and wind_lines_list then -- If table is being used
		for i = 1, #wind_rover_lists do -- Iterate it.
			local iterator = wind_rover_lists[i]
			local line = wind_lines_list[i]
			local windb = INT32_MIN
			local windt = INT32_MAX
			if line and line.frontsector then
				windb = line.frontsector.floorheight
				windt = line.frontsector.ceilingheight
			end

			for mobj in iterator() do
				if not mobj.player then continue end
				local check_height = mobj.z + (mobj.height >> 1)

				if (mobj.eflags & MFE_VERTICALFLIP and not ((windb > mobj.z + mobj.height) or windt < check_height))
				or not ((windt < mobj.z) or (windb > check_height)) then
					MFZWind(l, mobj, true)
				end
			end
		end
	end
end)
]]

addHook("PlayerThink", function(p)
--p.pflags = $ & PF_INVIS
	if not p.realmo then return end
	local mo = p.realmo

	if mo.snowing == nil then
		mo.snowing = 0
	elseif (mo.snowing) then
		mo.snowing = $ - 1
	else
		S_StopSoundByID(mo, sfx_s3kcel)
	end

	if not (p.mo and p.mo.valid) then return end
	mo = p.mo

	local sector = mo.subsector.sector

	if wind_rover_list[#sector] then
		local line = wind_lines_list[#sector]
		local windb = line.frontsector.floorheight
		local windt = line.frontsector.ceilingheight

		local check_height = mo.z + (mo.height >> 1)

		if line and (mo.eflags & MFE_VERTICALFLIP
		and not ((windb > mo.z + mo.height) or windt < check_height))
		or not ((windt < mo.z) or (windb > check_height)) then
			p.mrce.infreezingwind = 1
			MFZWind(line, p.mo, true)
		end
	else
		p.mrce.infreezingwind = 0
	end
end)

local MIN_DIST = 200 * FRACUNIT

addHook("MapThingSpawn", function(mo, mt)
	local trueangle = FixedAngle(mt.angle*FRACUNIT)
	mo.tag = mt.angle/360+1
	for ld in lines.tagged(mo.tag) do
		if ld.special == 6 then
			local ldd = R_PointToDist2(ld.v1.x, ld.v1.y, ld.v2.x, ld.v2.y)
			local ldsd = ld.frontside
			local ldst = ld.frontsector
			local target = P_SpawnMobj(ldsd.textureoffset, ldsd.rowoffset, ldst.floorheight, MT_MFZWINDSPAWNER)
			target.flags = MF_NOTHINK
			mo.target = target
			mo.newradius = FixedMul(ldd or FRACUNIT, MIN_DIST)
			mo.windspeed = ldd/FRACUNIT
			mo.extravalue1 = mo.windspeed-8
			mo.windmaxlen = abs(ldst.ceilingheight-ldst.floorheight)/FRACUNIT
			mo.timer = ldst.special
			mo.fuse = mo.timer
			mo.windamount = ldst.lightlevel
			mo.targetangle = R_PointToAngle2(max(mo.x, target.x), max(mo.y, target.y), min(mo.x, target.x), min(mo.y, target.y))
		end
	end
end, MT_MFZWINDSPAWNER)

-- Not really random, but what player in game will notice that snowflake is different?!
-- Speaking of waste of performance really...
local FRAME_RANDOMIZER = {
	[0] = A|FF_FULLBRIGHT,
	[1] = B|FF_FULLBRIGHT,
	[2] = C|FF_FULLBRIGHT,
}

addHook("MobjFuse", function(mo)
	if IsPlayerAroundBool(mo, mo.newradius or MIN_DIST, 4) then
		for i = 1, mo.windamount, 1 do
			local rx = P_RandomRange(mo.x >> FRACBITS, mo.target.x >> FRACBITS) << FRACBITS
			local ry = P_RandomRange(mo.y >> FRACBITS, mo.target.y >> FRACBITS) << FRACBITS
			local rz = P_RandomRange(mo.z >> FRACBITS, mo.target.z >> FRACBITS) << FRACBITS

			if R_PointToAngle2(mo.x, mo.y, rx, ry) < mo.targetangle then continue end
			local windspeed = P_RandomRange(mo.extravalue1, mo.windspeed)
			local wind = P_SpawnMobj(rx, ry, rz, MT_MFZWINDSNOW)
			wind.frame = FRAME_RANDOMIZER[i % 3]
			wind.momx = windspeed*cos(mo.angle)
			wind.momy = windspeed*sin(mo.angle)
			wind.momz = -(windspeed >> 4)
			wind.tics = mo.windmaxlen/windspeed
		end
	end
	mo.fuse = mo.timer
	return true
end, MT_MFZWINDSPAWNER)