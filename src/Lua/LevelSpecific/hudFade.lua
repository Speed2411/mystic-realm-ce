-- Teleport Transitions by Bloops
addHook("LinedefExecute", function(line, mo, sector)
	if mo and mo.valid then
		if not mo.othiusHudFade then
			if mo.othiusHudFade == nil then
				mo.othiusHudFade = 1
			else
				mo.othiusHudFade = 1
			end
		end
	end
end, "BLOOPS__HUDFADE_")

--simple, v is the drawer, num is the desired transparency, do a truth check with function before drawing with it!
local function adjustHUDTrans(v, num)
	local theShift = min(10,(v.localTransFlag()>>V_ALPHASHIFT)+num)
	if theShift >= 10 then
		return false
	end
	return (theShift<<V_ALPHASHIFT)
end

hud.add(function(v,p)
	if p.mo and p.mo.valid
	and p.mo.othiusHudFade ~= nil then
		local modTrans = p.mo.othiusHudFade
		if modTrans > 0 then
			modTrans = $*3
		else
			modTrans = abs($)
		end
		local hudTrans = adjustHUDTrans(v, modTrans)
		if hudTrans
		or hudTrans == 0 then
			local patch = v.cachePatch('YOUREWELCOMEOTHIUS')
			v.drawStretched(0, 0, v.width()*FU/patch.width, v.height()*FU/patch.height, patch, V_SNAPTOLEFT|V_SNAPTOTOP|V_NOSCALESTART|V_NOSCALEPATCH|hudTrans)
		end
	end
end, "game")

addHook("ThinkFrame", function()
	if consoleplayer and consoleplayer.mo
	and consoleplayer.mo.valid then
		if consoleplayer.mo.othiusHudFade ~= nil then
			consoleplayer.mo.othiusHudFade = max(-10, $-1)
			if consoleplayer.mo.othiusHudFade == -10 then
				consoleplayer.mo.othiusHudFade = nil
			end
		end
	end
end)