-- This source file is meant for all scripts for Mystic Realms

-- Prefab for Mystic Realms Zone, the crystal background
ScriptNodes["MYSTICREALMZONE_CRYSTALPREFAB"] = function(self, args, textarg)
	local minrad_arg = abs(args[0]) 							-- Min Radius Range
	local maxrad_arg = abs(args[1]) 							-- Max Radius Range
	local minhei_arg = args[2] 									-- Min Height Range
	local maxhei_arg = args[3] 									-- Max Height Range
	local maxscl_arg = abs(args[4]) 							-- Max Scale Range
	local rotate_arg = args[5] 									-- Rotation Speed
	local ammoun_arg = abs(args[6]) 							-- Amount

	local color_arg = textarg and
	string.upper(textarg or '') or nil						-- Color

	if color_arg and _G[color_arg] and skincolors[_G[color_arg]]
	and skincolors[_G[color_arg]] ~= SKINCOLOR_NONE then
		self.extravalue1 = _G[color_arg]
	else
		self.extravalue1 = SKINCOLOR_APPLE
		if color_arg then
			print("\x85".."WARNING:".."\x80".."Object #"..#mt.." has invalid skincolor indentificator!")
		end
	end

	local rel_dist = maxrad_arg - minrad_arg
	local dist_change = rel_dist/8

	local loops = ammoun_arg > 1 and (((360*18) / ammoun_arg)*ANG1)/18 or 0
	for i = 0, ammoun_arg, 1 do
		local loop = i*loops
		local radius = minrad_arg+P_RandomKey(ammoun_arg)*(rel_dist)/ammoun_arg
		local x = self.x + radius * cos(loop)
		local y = self.y + radius * sin(loop)
		local z = self.z + (minhei_arg+P_RandomKey(ammoun_arg)*(maxhei_arg-minhei_arg)/ammoun_arg) * FRACUNIT

		local crystal = P_SpawnMobj(x, y, z, MT_SPACECRYSTAL)
		crystal.scale = self.scale + (P_RandomKey(ammoun_arg)*(maxscl_arg)/ammoun_arg) * (FRACUNIT >> 5)
		crystal.extravalue1 = self.extravalue1
		crystal.cusval = rotate_arg*ANG1
		crystal.frame = max(min((radius-minrad_arg)/dist_change, 5), 0)|FF_SEMIBRIGHT|FF_PAPERSPRITE
	end

	P_RemoveMobj(self)
end