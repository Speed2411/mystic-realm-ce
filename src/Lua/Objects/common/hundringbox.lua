freeslot("MT_CONTICON", "S_CONTICON1")

local freshset = 0
local setcollect = 0

mobjinfo[MT_CONTICON] = {
--$Title Continue Medal
--$Sprite HUNDA0
--$Category Mystic Realm Special
--$NotAngled
	doomednum = 2218,
	radius = 28*FRACUNIT,
	height = 38*FRACUNIT,
}

local offset_matrix = {{2*FRACUNIT, 0},{0, 2*FRACUNIT},{0, -2*FRACUNIT},{-2*FRACUNIT, 0}}

addHook("MobjSpawn", function(mo)
	mo.origz = mo.z
	mo.scale = 2*FRACUNIT
	if not (multiplayer or modeattacking or gamecomplete) then
		mo.contindex = freshset + 1
		freshset = $ + 1
	end

	mo.overlays = {}

	-- Outline
	for i = 1,4 do
		local x, y = offset_matrix[i][1], offset_matrix[i][2]
		local overlay = P_SpawnMobjFromMobj(mo, 0, 0, 0, MT_OVERLAY)
		overlay.spritexoffset = x
		overlay.spriteyoffset = y
		overlay.target = mo
		overlay.color = SKINCOLOR_WHITE
		overlay.colorized = true
		overlay.dispoffset = -64
		overlay.flags2 = $ | MF2_LINKDRAW
		mo.overlays[i] = overlay
		-- TODO: Only use translation in 2.2.14
		overlay.translation = "WhiteFlash"
	end

	mo.dispoffset = 0
end, MT_CONTICON)

addHook("MobjThinker", function(mo)
	if not (mo and mo.valid) then return end

	local dist = INT32_MAX
	local angl = 0
	local temp
	local closestplayer
	if mo.speentime then
		mo.angle = $ + FixedAngle((60 - mo.fuse) * FRACUNIT)
		mo.z = $ + (2*FRACUNIT*P_MobjFlip(mo))
	else
		mo.angle = $ + FixedAngle(FRACUNIT)
		mo.z = mo.origz + 8 * (abs(sin(FixedAngle(leveltime*4*FRACUNIT))) * P_MobjFlip(mo))
	end
	for p in players.iterate do
		if not p.valid or p.bot or p.spectator then continue end
		if not (p.mo and p.mo.valid) then continue end
		if multiplayer and p.playerstate ~= PST_LIVE then continue end

		temp = FixedHypot(FixedHypot(p.mo.x - mo.x, p.mo.y - mo.y), p.mo.z - mo.z)

		if temp < dist then
			closestplayer = p
			dist = temp
		end

		if closestplayer == nil or skins[closestplayer.skin].sprites[SPR2_XTRA].numframes == 0 then
			-- Closest player not found (no players in game?? may be empty dedicated server!), or does not have correct sprite.
			mo.sprite = SPR_UNKN
			mo.frame = FF_FULLBRIGHT|FF_PAPERSPRITE|B
			return
		end

		if mo.speentime then
			closestplayer = mo.speentime
		end

		mo.skin = closestplayer.mo.skin
		mo.sprite = SPR_PLAY
		mo.sprite2 = SPR2_XTRA
		mo.frame = FF_FULLBRIGHT|FF_PAPERSPRITE|C
		mo.color = closestplayer.skincolor
		mo.colorized = closestplayer.mo.colorized
		mo.blendmode = closestplayer.mo.blendmode
	end

	if mo.fuse and mo.fuse < 36 then
		mo.frame = C|FF_FULLBRIGHT|FF_PAPERSPRITE|TR_TRANS90-((mo.fuse*FRACUNIT)/4)
	end

	local dif = mo.angle-R_PointToAngle(mo.x, mo.y)
	-- Flipping sprite to show full 360 rotation and not just 180
	if dif >= ANGLE_180 and dif <= ANGLE_MAX then
		mo.frame = $|FF_HORIZONTALFLIP
	end

	for i = 1, 4 do
		local overlay = mo.overlays[i]
		if not overlay then continue end

		overlay.skin = mo.skin
		overlay.state = mo.state
		overlay.sprite = mo.sprite
		overlay.sprite2 = mo.sprite2
		overlay.frame = mo.frame
	end

	if not (multiplayer or modeattacking or gamecomplete) then
		if (setcollect & (1 << (mo.contindex - 1))) and not mo.speentime then
			P_RemoveMobj(mo)
		end
	end
end, MT_CONTICON)

local contcount = 0
local contmap

addHook("TouchSpecial", function(mo, toucher)
	if mo and mo.valid and toucher and toucher.valid and toucher.player and mo.health then
		if mo.speentime then return true end
		local p = toucher.player
		if ((mapheaderinfo[gamemap].lvlttl == "Dimension Warp") or (mapheaderinfo[gamemap].lvlttl == "Primordial Abyss")) or modeattacking then
			P_AddPlayerScore(p, 2500)
			S_StartSound(toucher, sfx_s1c5)
		elseif multiplayer or gamecomplete or p.lives == INFLIVES then
			P_GivePlayerRings(p, 100)
			if p.skipscrap ~= nil then
				p.skipscrap = $ + 150
			end
			S_StartSound(p.mo, sfx_kc33)
		else
			if ultimatemode then
				if contcount < 4 then
					contcount = $ + 1
					S_StartSound(toucher, sfx_token)
				else
					S_StartSound(nil, sfx_s23f, p)
					contcount = 0
					p.continues = min($ + 1, 99)
				end
			elseif not modeattacking then
				if contcount < 2 then
					contcount = $ + 1
					S_StartSound(toucher, sfx_token)
				else
					contcount = 0
					S_StartSound(nil, sfx_s23f, p)
					p.continues = min($ + 1, 99)
				end
			end
		end
		if not (multiplayer or modeattacking or (p.lives == INFLIVES) or gamecomplete) then
			setcollect = $|(1 << (mo.contindex - 1))
		end
		mo.speentime = p
		mo.fuse = 50
		mo.scalespeed = FRACUNIT / 18
		mo.destscale = $ * 99
		mo.flags = $|MF_NOCLIP|MF_NOCLIPHEIGHT
		P_AddPlayerScore(p, 5000)
		return true
	end
end, MT_CONTICON)

addHook("MapChange", function(map)
	if (multiplayer or modeattacking or gamecomplete) then return end
	if contmap == nil then
		contmap = map
	elseif contmap == map then
		freshset = 0
	else
		freshset = 0
		setcollect = 0
	end
end)

addHook("NetVars", function(net)
	contmap = net($)
	freshset = net($)
	setcollect = net($)
	contcount = net($) --in no way should this be necessary since these are unused in netgames, but there's a fucking desync SOMEWHERE AND I'M GOING TO FUCKING KILL IT
end)

-- Properly fits the player's 1up icon into the Final Demo monitor
-- Mostly based off of 2.2.9's source code
function A_1upThinker(actor, var1, var2)
	if not (actor and actor.valid) then return end

	local dist = INT32_MAX
	local temp
	local closestplayer

	for player in players.iterate do
		if not player.valid or player.bot or player.spectator then
			continue
		end

		if not (player.mo and player.mo.valid) then
			continue
		end

		if (netgame or multiplayer) and player.playerstate ~= PST_LIVE then
			continue
		end

		temp = FixedHypot(player.mo.x-actor.x, player.mo.y-actor.y)

		if temp < dist then
			closestplayer = player
			dist = temp
		end

		if closestplayer == nil or skins[closestplayer.skin].sprites[SPR2_LIFE].numframes == 0 then
			-- Closest player not found (no players in game?? may be empty dedicated server!), or does not have correct sprite.
			if actor.tracer then
				local tracer = actor.tracer
				actor.tracer = nil
				P_RemoveMobj(tracer)
			end
			return
		end

		-- We're using the overlay, so use the overlay 1up box (no text)
		actor.sprite = SPR_TV1P

		if not actor.tracer then
			actor.tracer = P_SpawnMobjFromMobj(actor, 0, 0, 0, MT_OVERLAY)
			actor.tracer.target = actor
			actor.tracer.skin = closestplayer.mo.skin -- required here to prevent spr2 default showing stand for a single frame
			actor.tracer.state = actor.info.seestate

			actor.tracer.spriteyoffset = 8*FRACUNIT
			actor.tracer.spritexscale = 1*FRACUNIT
			actor.tracer.spriteyscale = 1*FRACUNIT

			-- The overlay is going to be one tic early turning off and on
			-- because it's going to get its thinker run the frame we spawned it.
			-- So make it take one tic longer if it just spawned
			actor.tracer.tics = $+1
		end

		if MRCE_isHyper(closestplayer) then
			if closestplayer.mrce.canhyper then
				actor.tracer.color = SKINCOLOR_MRCEHYPER1
			elseif closestplayer.mrce.ultrastar then
				actor.tracer.color = SKINCOLOR_MRCEHYPER2
			end
		else
			actor.tracer.color = closestplayer.mo.color
		end
		actor.tracer.skin = closestplayer.mo.skin
	end
end
