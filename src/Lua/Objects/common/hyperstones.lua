freeslot(
    "S_LIGHTNING_STONE",
    "S_FIRE_STONE",
    "S_WATER_STONE",
    "SPR_KYST"
)

mobjinfo[MT_LIGHTNING_STONE] = {
--$Name Elemental Shard
--$Sprite KYSTA0
--$Category Mystic Realm Special
--$NotAngled
	doomednum = 3110,
	spawnstate = S_LIGHTNING_STONE,
	radius = 28*FRACUNIT,
	height = 38*FRACUNIT,
	flags = MF_NOGRAVITY|MF_SPECIAL
}

states[S_LIGHTNING_STONE] = {
	sprite = SPR_KYST,
	frame = A,
}

addHook("TouchSpecial", function(mo, toucher)
	if mo and mo.valid then
		if mapheaderinfo[gamemap].lvlttl ~= "Starlight Palace" then
			if not (mrce.elemshards & (1 << (0))) then
				if not multiplayer then
					GlobalBanks_Array[0] = $ | (1 << (15))
				end
				mrce.elemshards = $ | (1 << (0))
				S_StartSound(nil, sfx_cdfm63)
			end
		else
			return true
		end
	end
end, MT_LIGHTNING_STONE)

addHook("MobjThinker", function(mo)
	if mo and mo.valid then
		if leveltime > 3 then
			if (mrce.elemshards & (1 << (0)))
			and mapheaderinfo[gamemap].lvlttl ~= "Starlight Palace" then
				P_RemoveMobj(mo)
			elseif mapheaderinfo[gamemap].lvlttl == "Starlight Palace"
			and not (mrce.elemshards & (1 << (0))) then
				P_RemoveMobj(mo)
			end
		end
	end
end, MT_LIGHTNING_STONE)

mobjinfo[MT_FIRE_STONE] = {
	doomednum = 3111,
	spawnstate = S_FIRE_STONE,
	radius = 28*FRACUNIT,
	height = 38*FRACUNIT,
	flags = MF_NOGRAVITY|MF_SPECIAL
}

states[S_FIRE_STONE] = {
	sprite = SPR_KYST,
	frame = B,
}

addHook("TouchSpecial", function(mo, toucher)
	if mo and mo.valid then
		if mapheaderinfo[gamemap].lvlttl ~= "Starlight Palace" then
			if not (mrce.elemshards & (1 << (1))) then
				if not multiplayer then
					GlobalBanks_Array[0] = $ | (1 << (16))
				end
				mrce.elemshards = $ | (1 << (1))
				S_StartSound(nil, sfx_cdfm63)
			end
		else
			return true
		end
	end
end, MT_FIRE_STONE)

addHook("MobjThinker", function(mo)
	if mo and mo.valid then
		if leveltime > 3 then
			if (mrce.elemshards & (1 << (1)))
			and mapheaderinfo[gamemap].lvlttl ~= "Starlight Palace" then
				P_RemoveMobj(mo)
			elseif mapheaderinfo[gamemap].lvlttl == "Starlight Palace"
			and not (mrce.elemshards & (1 << (1))) then
				P_RemoveMobj(mo)
			end
		end
	end
end, MT_FIRE_STONE)

mobjinfo[MT_WATER_STONE] = {
	doomednum = 3112,
	spawnstate = S_WATER_STONE,
	radius = 28*FRACUNIT,
	height = 38*FRACUNIT,
	flags = MF_NOGRAVITY|MF_SPECIAL
}

states[S_WATER_STONE] = {
	sprite = SPR_KYST,
	frame = C,
}

addHook("TouchSpecial", function(mo, toucher)
	if mo and mo.valid then
		if mapheaderinfo[gamemap].lvlttl ~= "Starlight Palace" then
			if not (mrce.elemshards & (1 << (2))) then
				if not multiplayer then
					GlobalBanks_Array[0] = $ | (1 << (17))
				end
				mrce.elemshards = $ | (1 << (2))
				S_StartSound(nil, sfx_cdfm63)
			end
		else
			return true
		end
	end
end, MT_WATER_STONE)

addHook("MobjThinker", function(mo)
	if mo and mo.valid then
		if leveltime > 3 then
			if (mrce.elemshards & (1 << (2)))
			and mapheaderinfo[gamemap].lvlttl ~= "Starlight Palace" then
				P_RemoveMobj(mo)
			elseif mapheaderinfo[gamemap].lvlttl == "Starlight Palace"
			and not (mrce.elemshards & (1 << (2))) then
				P_RemoveMobj(mo)
			end
		end
	end
end, MT_WATER_STONE)