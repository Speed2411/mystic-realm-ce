--[[
MRCE Lua HUD

(C) 2020-2022 by K. "ashifolfi" J.
]]

local anim_percent
local anim_percent2
local anim_capped
local anim_capped2
local anim_ticker = 0
local anim_ticker2 = 0
local slidein = "no"
local hticker = 0

local TICRATE = TICRATE
local FRACUNIT = FRACUNIT
local FRACBITS = FRACBITS
local FU = FU

local ease_outquart = ease.outquart

local max = max
local min = min

local draw
local ipairs = ipairs

local G_TicsToCentiseconds = G_TicsToCentiseconds
local G_TicsToMinutes = G_TicsToMinutes
local P_RandomRange = P_RandomRange
local tostring = tostring


if not MarioSkins then
	rawset(_G, "MarioSkins", {})
end

local function HudToggle(player, arg)
    if arg and player and player.mrce then
        if arg == "1" or arg == "on" or arg == "default" or arg == "true" or arg == "normal" or arg == "yes" then
			player.mrce.hud = 1
			if player.mrce.constext == 0 then
				CONS_Printf(player, "MRCE Custom hud enabled")
			end
			if io and player == consoleplayer then
				local file = io.openlocal("client/mrce/hud.dat", "w+")
				file:write(arg)
				file:close()
			end
		elseif arg == "off" or arg == "0" or arg == "no" or arg == "false" or arg == "disable" then
			player.mrce.hud = 0
			if player.mrce.constext == 0 then
				CONS_Printf(player, "MRCE Custom hud disabled")
			end
			if io and player == consoleplayer then
				local file = io.openlocal("client/mrce/hud.dat", "w+")
				file:write(arg)
				file:close()
			end
        end
    elseif player.mrce.constext == 0 then
        CONS_Printf(player, "Toggle MRCE lua hud. Note many effects are used by the custom hud, and are lost when disabled, so BEWARE")
    end
end

COM_AddCommand("mr_hud", function(player, arg)
	HudToggle(player, arg)
end)

-- I am not doing everything.
-- PLEASE JUST AVOID OVERUSING C-FUNCTIONS LIKE v.cachePatch when you don't have to. They run every tic, ya know?
local MRHSCORE, MRHTIME, MRHRINGS, MRHCOINS, MRHRRING, MRHRCOIN, MRCEFNTS, MRCEFNTP, MRHPRING
-- not literal globals, just variables for global hud use.
-- While yes 27*FRACUNIT thousand times won't cause performance issues
-- Just please, these constantly have to be calculated
local HUD_GLOBAL_X = -300*FU
local HUD_GLOBAL_TWZ = 27*FRACUNIT
-- Same with flags, bit-wise operations are still instructions
local TOP_LEFT_FLAGS = V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS
-- Not gonna lie, there could be made a lot of optimalizations, pretty sure font drawer is also like that.

local TIC_FRAC = FU / TICRATE

local function DrawMRCEHUD(v, p, cam, ticker, endticker)
	if p.spectator then return end
	if not p.realmo then return end
	local replaced = {"rings", "time", "score", "lives"}

	if p.shouldhud == false then
		for k,v in ipairs(replaced) do
			hud.disable(v)
		end
		return
	end

	if p.mrce.hud == 0 then
		for k,v in ipairs(replaced) do
			hud.enable(v)
		end
		return
	end

	if p.exiting <= 50
	and p.exiting > 0 then
		if anim_ticker2 > 44 then
			anim_ticker2 = 44
		end
		anim_ticker2 = $ - 1
	end

	if not MRHSCORE then
		MRHRINGS = v.cachePatch("MRHRINGS")
		MRHCOINS = v.cachePatch("MRHCOINS")
		MRHRRING = v.cachePatch("MRHRRING")
		MRHRCOIN = v.cachePatch("MRHRCOIN")
		MRHSCORE = v.cachePatch("MRHSCORE")
		MRHTIME = v.cachePatch("MRHTIME")
		MRCEFNTS = v.cachePatch("MRCEFNTS")
		MRCEFNTP = v.cachePatch("MRCEFNTP")

		-- purple
		MRHPRING = v.cachePatch("MRHPRING")

		draw = v.draw
	end

	anim_capped = max(min(anim_ticker, TICRATE), 0)
	anim_percent = TIC_FRAC * anim_capped

	anim_capped2 = max(min(anim_ticker2, TICRATE), 0)
	anim_percent2 = TIC_FRAC * anim_capped2

	if (p.realmo) and (p.hudstyle == "srb2" or p.hudstyle == nil) and not (p.mo.skin == "speccy" and p.speccy) and not ((p.mo.skin == "samus") or (p.mo.skin == "basesamus")) and not (p.mo.skin == "duke") and not (srb2p) and not (maptol & TOL_NIGHTS) and not (G_IsSpecialStage(gamemap)) and gamemap ~= 99 and CHUD == nil and customhud == nil
	and p == displayplayer and p.mrce.hud == 1 then
		-- Colormap is also C call having to be made.
		local colormap = v.getColormap(p.realmo.skin, p.realmo.color)
		local x_exp_ease = ease_outquart(anim_percent, -300 , 20)
		local fx_x_exp_es = ease_outquart(anim_percent, HUD_GLOBAL_X, 75*FRACUNIT)

		-- Score
		draw(x_exp_ease, 10, MRHSCORE, TOP_LEFT_FLAGS, colormap)
		DrawMotdString(v, fx_x_exp_es, 10*FRACUNIT, FRACUNIT, tostring(p.score), "MRCEFNT", TOP_LEFT_FLAGS)
		-- Time
		draw(x_exp_ease, 26, MRHTIME, TOP_LEFT_FLAGS, colormap)
		if G_TicsToMinutes(p.realtime) < 10 and mapheaderinfo[gamemap].lvlttl ~= "Dimension Warp" then
			DrawMotdString(v, fx_x_exp_es, HUD_GLOBAL_TWZ, FRACUNIT, "0", "MRCEFNT", TOP_LEFT_FLAGS)
			DrawMotdString(v, ease_outquart(anim_percent, HUD_GLOBAL_X, 83*FRACUNIT), HUD_GLOBAL_TWZ, FRACUNIT, G_TicsToMinutes(p.realtime), "MRCEFNT", TOP_LEFT_FLAGS)
		elseif G_TicsToMinutes(p.realtime) >= 10 and mapheaderinfo[gamemap].lvlttl ~= "Dimension Warp" then
			DrawMotdString(v, fx_x_exp_es, HUD_GLOBAL_TWZ, FRACUNIT, G_TicsToMinutes(p.realtime), "MRCEFNT", TOP_LEFT_FLAGS)
		elseif mapheaderinfo[gamemap].lvlttl == "Dimension Warp" then
			DrawMotdString(v, fx_x_exp_es, HUD_GLOBAL_TWZ, FRACUNIT, tostring(p.warpminutes), "MRCEFNT", TOP_LEFT_FLAGS)
		end
		if G_TicsToSeconds(p.realtime) < 10 and mapheaderinfo[gamemap].lvlttl ~= "Dimension Warp" then
			DrawMotdString(v, ease_outquart(anim_percent, HUD_GLOBAL_X, 99*FRACUNIT), HUD_GLOBAL_TWZ, FRACUNIT, "0", "MRCEFNT", TOP_LEFT_FLAGS)
			DrawMotdString(v, ease_outquart(anim_percent, HUD_GLOBAL_X, 107*FRACUNIT), HUD_GLOBAL_TWZ, FRACUNIT, G_TicsToSeconds(p.realtime), "MRCEFNT", TOP_LEFT_FLAGS, 1)
		elseif G_TicsToSeconds(p.realtime) >= 10 and mapheaderinfo[gamemap].lvlttl ~= "Dimension Warp" then
			DrawMotdString(v, ease_outquart(anim_percent, HUD_GLOBAL_X, 99*FRACUNIT), HUD_GLOBAL_TWZ, FRACUNIT, G_TicsToSeconds(p.realtime), "MRCEFNT", TOP_LEFT_FLAGS, 1)
		elseif mapheaderinfo[gamemap].lvlttl == "Dimension Warp" then
			DrawMotdString(v, ease_outquart(anim_percent, HUD_GLOBAL_X, 99*FRACUNIT), HUD_GLOBAL_TWZ, FRACUNIT, tostring(p.warpseconds), "MRCEFNT", TOP_LEFT_FLAGS, 1)
		end
		if G_TicsToCentiseconds(p.realtime) < 10 and mapheaderinfo[gamemap].lvlttl ~= "Dimension Warp" then
			DrawMotdString(v, ease_outquart(anim_percent, HUD_GLOBAL_X, 123*FRACUNIT), HUD_GLOBAL_TWZ, FRACUNIT, "0", "MRCEFNT", TOP_LEFT_FLAGS)
			DrawMotdString(v, ease_outquart(anim_percent, HUD_GLOBAL_X, 131*FRACUNIT), HUD_GLOBAL_TWZ, FRACUNIT, G_TicsToCentiseconds(p.realtime), "MRCEFNT", TOP_LEFT_FLAGS, 1)
		elseif G_TicsToCentiseconds(p.realtime) >= 10 and mapheaderinfo[gamemap].lvlttl ~= "Dimension Warp" then
			DrawMotdString(v, ease_outquart(anim_percent, HUD_GLOBAL_X, 123*FRACUNIT), HUD_GLOBAL_TWZ, FRACUNIT, G_TicsToCentiseconds(p.realtime), "MRCEFNT", TOP_LEFT_FLAGS, 1)
		elseif mapheaderinfo[gamemap].lvlttl == "Dimension Warp" then
			DrawMotdString(v, ease_outquart(anim_percent, HUD_GLOBAL_X, 123*FRACUNIT), HUD_GLOBAL_TWZ, FRACUNIT, tostring(p.warpcentiseconds), "MRCEFNT", TOP_LEFT_FLAGS, 1)
		end
		draw(ease_outquart(anim_percent, -300 , 91), 27, MRCEFNTS, TOP_LEFT_FLAGS)
		draw(ease_outquart(anim_percent, -300 , 115), 27, MRCEFNTP, TOP_LEFT_FLAGS)
		-- Rings
		if not ultimatemode
		or mapheaderinfo[gamemap].lvlttl == "Dimension Warp"
		and not MarioSkins[p.mo.skin] and not mariomode then --normal hud draw for normal characters
		local getrings = p.mo.skin == "xian" and p.xian.rings or p.rings
			draw(ease_outquart(anim_percent2, -300 , 20), 42, p.mo.pw_combine and MRHPRING or MRHRINGS, TOP_LEFT_FLAGS, colormap)
			DrawMotdString(v, ease_outquart(anim_percent2, HUD_GLOBAL_X, 75*FRACUNIT), 42*FRACUNIT, FRACUNIT, tostring(getrings), "MRCEFNT", TOP_LEFT_FLAGS)
		elseif not ultimatemode
		or mapheaderinfo[gamemap].lvlttl == "Dimension Warp" then  --mario time, oh yeah! I'd like to swap the rings graphic out for a coins version where appropriate, just need a graphic for it
			draw(ease_outquart(anim_percent2, -300 , 20), 42, MRHCOINS, TOP_LEFT_FLAGS, colormap)
			DrawMotdString(v, ease_outquart(anim_percent2, HUD_GLOBAL_X, 75*FRACUNIT), 42*FRACUNIT, FRACUNIT, tostring(p.rings), "MRCEFNT", TOP_LEFT_FLAGS)
		end
		if (p.rings == 0 and (leveltime/5%2) and not ultimatemode and p.mo.skin ~= "xian") or (mapheaderinfo[gamemap].lvlttl == "Dimension Warp" and p.rings <= 10 and (leveltime/5%2)) or (p.mo.skin == "supersonic" and p.rings <= 10 and (leveltime/5%2))
		and (MarioSkins and not MarioSkins[p.mo.skin]) and not mariomode then
			draw(ease_outquart(anim_percent2, -300 , 20), 42, MRHRRING, TOP_LEFT_FLAGS, colormap)
		elseif (MarioSkins and MarioSkins[p.mo.skin]) and (mapheaderinfo[gamemap].lvlttl == "Dimension Warp" and p.rings <= 10 and (leveltime/5%2))
		or mariomode and not MarioSkins[p.mo.skin] then
			draw(ease_outquart(anim_percent2, -300 , 20), 42, MRHRCOIN, TOP_LEFT_FLAGS, colormap)
		end
		if p.mrce.hud == 1 then
			hud.disable("rings")
			hud.disable("time")
			hud.disable("score")
		end
	end
end
hud.add(DrawMRCEHUD, "game")

addHook("MapLoad", function()
  anim_ticker = 0
  anim_ticker2 = 0
end)

hud.add(function(v,p,ticker,endticker)
	if p.exiting then return end
	if modeattacking then return end
	if ticker == 1 then
		anim_ticker = 0
		anim_ticker2 = 0
	end
	if ticker > 70 then
		anim_ticker = $ + 1
		anim_ticker2 = $ + 1
	end
end, "titlecard")

addHook("ThinkFrame", function()
	if displayplayer and displayplayer.exiting then return end
	if not modeattacking then return end
	if leveltime == 1 then
		anim_ticker = 0
		anim_ticker2 = 0
	elseif leveltime > 35 and leveltime < 80 then
		anim_ticker = $ + 1
		anim_ticker2 = $ + 1
	end
end)

addHook("PlayerThink", function(p)
	if (mapheaderinfo[gamemap].lvlttl ~= "Dimension Warp") then return end
	if not (leveltime%35)
	or (leveltime%TICRATE == TICRATE/5) or (leveltime%TICRATE == ((TICRATE/5) * 3)) or (leveltime%TICRATE == ((TICRATE/5) * 2)) or (leveltime%TICRATE == ((TICRATE/5) * 4)) then
		p.warpminutes =  P_RandomRange(10, 99)
		p.warpseconds =  P_RandomRange(10, 99)
		p.warpcentiseconds =  P_RandomRange(10, 99)
	end
end)