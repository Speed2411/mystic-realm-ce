local emblemlist = {}
local numembs = 0
local offset = 400
local offset2 = 360
local offsetresettimer = 0
local offsetresettimer2 = 0
local tabresettimer = 2
local touchedresettimer = 25
local tabMenuOpened = nil
local emblemsupdated = 0

local FRACUNIT = FRACUNIT
local FRACBITS = FRACBITS
local MF2_DONTDRAW = MF2_DONTDRAW
local FF_FRAMEMASK = FF_FRAMEMASK


local ipairs = ipairs


addHook("PostThinkFrame", function()
	if multiplayer then return end
	if emblemsupdated < 5 then emblemsupdated = $ + 1 return end
	if emblemsupdated == 5 then
		numembs = 0
		emblemlist = {}
		for mapemblem in mobjs.iterate() do
			if mapemblem.valid and mapemblem.type == MT_EMBLEM then
				numembs = $ + 1
				local emblem = {frame = mapemblem.frame, color = mapemblem.color, origmobj = mapemblem}
				table.insert(emblemlist, emblem)
			end
		end
		emblemsupdated = 6
	end
end)

addHook("PlayerSpawn", function()
	if multiplayer then return end
	emblemsupdated = 0
end)

addHook("TouchSpecial", function(emblem, pmo)
	if multiplayer then return end
	pmo.player.changeoffset = 1
	offsetresettimer = touchedresettimer
	emblemsupdated = 0
end, MT_EMBLEM)

addHook("TouchSpecial", function(key, pmo)
	if multiplayer then return end
	pmo.player.changeoffset2 = 1
	offsetresettimer2 = touchedresettimer
end, MT_LIGHTNING_STONE)

addHook("TouchSpecial", function(key, pmo)
	if multiplayer then return end
	pmo.player.changeoffset2 = 1
	offsetresettimer2 = touchedresettimer
end, MT_FIRE_STONE)

addHook("TouchSpecial", function(key, pmo)
	if multiplayer then return end
	pmo.player.changeoffset2 = 1
	offsetresettimer2 = touchedresettimer
end, MT_WATER_STONE)

addHook("ThinkFrame", function()
	if multiplayer then return end
	for p in players.iterate do
		if not p.bot and p.valid then
			if tabMenuOpened == true then
				p.changeoffset = 1
				p.changeoffset2 = 1
				offsetresettimer = 2
				tabMenuOpened = false
			end
			if p.changeoffset then
				if offset > 210 then
					offset = $ - 10
				end
				if offset == 210 and offsetresettimer then
					offsetresettimer = $ - 1
				end
				if not offsetresettimer then
					p.changeoffset = 0
				end
			else
				if offset < 400 then
					offset = $ + 20
				end
			end
			if p.changeoffset2 then
				if offset2 > 280 then
					offset2 = $ - 10
				end
				if offset2 == 280 and offsetresettimer2 then
					offsetresettimer2 = $ - 1
				end
				if not offsetresettimer2 then
					p.changeoffset2 = 0
				end
			else
				if offset2 < 360 then
					offset2 = $ + 20
				end
			end
		end
	end
end)

local BG, UNCOLLECTEDKEY, KEY1, KEY2, KEY3, KEY1S, KEY2S, KEY3S
local UKE1_WIDTH, UKE2_WIDTH, UKE3_WIDTH, KEY1_WIDTH, KEY2_WIDTH, KEY3_WIDTH

local UNCOLLECTED_EMB, UNCOLLECTED_EMB_WIDTH

local GLOBAL_FLAGS = V_SNAPTORIGHT|V_SNAPTOTOP
local GLOBAL_NET_FLAGS = V_SNAPTOLEFT|V_SNAPTOTOP
local KEY_SCALE = FRACUNIT/3
local KEY_Y = 39 << FRACBITS
local BG_SCALE = 2*FRACUNIT/3

hud.add(function(d, p)
	-- No need to call the C function constantly.
	-- If mod gets remove, BG will get removed anyway - making it cache again
	if not BG then
		BG = d.cachePatch("EMBBG")
		KEY1 = d.getSpritePatch("KYST", A)
		KEY2 = d.getSpritePatch("KYST", B)
		KEY3 = d.getSpritePatch("KYST", C)
		UNCOLLECTEDKEY = d.cachePatch("KEY0L")
		-- NETGAME SCORE TABLE
		KEY1S = d.cachePatch("KEY1S")
		KEY2S = d.cachePatch("KEY2S")
		KEY3S = d.cachePatch("KEY3S")
		-- EMBLEM
		UNCOLLECTED_EMB = d.cachePatch("UNCEMB")

		-- Offloading calculations
		local KEY_OFFSET_1 = 6 << FRACBITS
		local KEY_OFFSET_2 = 18 << FRACBITS
		local KEY_OFFSET_3 = 30 << FRACBITS
		local UKEY_WIDTHNESS = ((UNCOLLECTEDKEY.width / 6) << FRACBITS)
		UKE1_WIDTH = UKEY_WIDTHNESS + KEY_OFFSET_1
		UKE2_WIDTH = UKEY_WIDTHNESS + KEY_OFFSET_2
		UKE3_WIDTH = UKEY_WIDTHNESS + KEY_OFFSET_3
		KEY1_WIDTH = ((KEY1.width / 6) << FRACBITS) + KEY_OFFSET_1
		KEY2_WIDTH = ((KEY2.width / 6) << FRACBITS) + KEY_OFFSET_2
		KEY3_WIDTH = ((KEY3.width / 6) << FRACBITS) + KEY_OFFSET_3
		UNCOLLECTED_EMB_WIDTH = (UNCOLLECTED_EMB.width / 6)*(10*FRACUNIT)
	end

	if multiplayer then return end
	if usedCheats then return end
	local loffset = offset
	local soffset = offset2
	local FIXED_SOFFSET = soffset*FRACUNIT

	-- Why is this here? It is unused.
	-- local key1 = d.getSpritePatch("KYST", A)
	-- local key2 = d.getSpritePatch("KYST", B)
	-- local key3 = d.getSpritePatch("KYST", C)


	if numembs > 0 then
		d.drawScaled(loffset*BG_SCALE + 65 << FRACBITS, 17 << FRACBITS, BG_SCALE, BG, GLOBAL_FLAGS)
	end
	if mrce.elemshards > 0 then
		d.drawScaled(soffset*BG_SCALE + 90 << FRACBITS, 36 << FRACBITS, BG_SCALE, BG, GLOBAL_FLAGS)
	end


	if (not (GlobalBanks_Array[0] & (1 << (15)))) and mrce.elemshards > 0 then
		d.drawScaled(FIXED_SOFFSET + UKE1_WIDTH, KEY_Y, KEY_SCALE, UNCOLLECTEDKEY, GLOBAL_FLAGS, emblemcolor)
	elseif (GlobalBanks_Array[0] & (1 << (15))) then
		d.drawScaled(FIXED_SOFFSET + KEY1_WIDTH, KEY_Y, KEY_SCALE, KEY1, GLOBAL_FLAGS, emblemcolor)
	end
	if (not (GlobalBanks_Array[0] & (1 << (16)))) and mrce.elemshards > 0 then
		d.drawScaled(FIXED_SOFFSET + UKE2_WIDTH, KEY_Y, KEY_SCALE, UNCOLLECTEDKEY, GLOBAL_FLAGS, emblemcolor)
	elseif (GlobalBanks_Array[0] & (1 << (16))) then
		d.drawScaled(FIXED_SOFFSET + KEY2_WIDTH, KEY_Y, KEY_SCALE, KEY2, GLOBAL_FLAGS, emblemcolor)
	end
	if (not (GlobalBanks_Array[0] & (1 << (17)))) and mrce.elemshards > 0 then
		d.drawScaled(FIXED_SOFFSET + UKE3_WIDTH, KEY_Y, KEY_SCALE, UNCOLLECTEDKEY, GLOBAL_FLAGS, emblemcolor)
	elseif (GlobalBanks_Array[0] & (1 << (17))) then
		d.drawScaled(FIXED_SOFFSET + KEY3_WIDTH, KEY_Y, KEY_SCALE, KEY3, GLOBAL_FLAGS, emblemcolor)
	end

	for embnum, emblem in ipairs(emblemlist) do
		local emblempatch = d.getSpritePatch("EMBM", emblem.frame & FF_FRAMEMASK)
		local uncollectedemb = UNCOLLECTED_EMB
		local emblemcolor = d.getColormap(TC_DEFAULT, emblem.color)
		d.drawScaled((loffset*BG_SCALE + UNCOLLECTED_EMB_WIDTH), 20 << FRACBITS, KEY_SCALE, UNCOLLECTED_EMB, GLOBAL_FLAGS, emblemcolor)
		if (emblem.frame & FF_TRANSMASK) or (not emblem.origmobj.valid) or ((emblem.origmobj and (emblem.origmobj.flags2 & MF2_DONTDRAW)) or (emblem.origmobj and (emblem.origmobj.state == S_INVISIBLE))) then
			d.drawScaled((loffset*BG_SCALE + ((emblempatch.width / 6)*(10 << FRACBITS))), 20 << FRACBITS, KEY_SCALE, emblempatch, GLOBAL_FLAGS, emblemcolor)
		end
		loffset = $ + 32
		soffset = $ + 32
	end
end, "game")

hud.add(function(d, p)
	tabMenuOpened = true
	offsetresettimer = tabresettimer
	offsetresettimer2 = tabresettimer
	if netgame then
		if (mrce.elemshards & (1 << (0))) then
			d.draw(20, 13, KEY1S, GLOBAL_NET_FLAGS)
		end
		if (mrce.elemshards & (1 << (1))) then
			d.draw(30, 13, KEY2S, GLOBAL_NET_FLAGS)
		end
		if (mrce.elemshards & (1 << (2))) then
			d.draw(40, 13, KEY3S, GLOBAL_NET_FLAGS)
		end
	end
end, "scores")
