--	DISPLAY OF EMERALDS
--	Scripted by AceLite
--	Contributed from Sonic Adventure Style Pack
--

local emerald = {EMERALD1, EMERALD2, EMERALD3, EMERALD4, EMERALD5, EMERALD6, EMERALD7}
local emerald_max = EMERALD1|EMERALD2|EMERALD3|EMERALD4|EMERALD5|EMERALD6|EMERALD7

local V_ALPHASHIFT = V_ALPHASHIFT
local TC_DEFAULT = TC_DEFAULT
local ANGLE_225 = ANGLE_225
local FRACUNIT = FRACUNIT
local FRACBITS = FRACBITS
local ANG1 = ANG1

local circle_bool = false
local circle_scale = 0
local circle_split = (360/#emerald)*ANG1
local circle_size = 34
local circle_timing = FRACUNIT/10
local circle_start = -(FRACUNIT << 1)
local circle_end = FRACUNIT << 1

local black_em_alpha = ANG1*(360/30)
local black_em_delta = -8*TICRATE
local black_em_animt = 15
local black_em_goal = -black_em_delta+black_em_animt
local black_em_timer = 0
local black_em_color = 1

local em_x = (151+8) << FRACBITS -- base+new_offset
local em_y = (78+9) << FRACBITS -- base+new_offset
local em_s = FRACUNIT*(3/2)

local bem_x = 200 << FRACBITS
local bem_y = 9 << FRACBITS
local bem_s = FRACUNIT >> 1

local draw_scaled
local patches_untilized = true
local patches = {}

local hud_enable = hud.enable
local hud_disable = hud.disable
local getColormap
local RandomRange

local ease_linear = ease.linear

local FixedMul = FixedMul
local FixedDiv = FixedDiv

local sin = sin
local cos = cos
local abs = abs

local hyper_pal = {
	SKINCOLOR_SUPERAETHER2,
	SKINCOLOR_SUPERSAPPHIRE2,
	SKINCOLOR_SUPERBUBBLEGUM2,
	SKINCOLOR_SUPERMINT2,
	SKINCOLOR_SUPERRUBY2,
	SKINCOLOR_SUPERWAVE2,
	SKINCOLOR_SUPERCOPPER2,
	SKINCOLOR_SUPERAETHER2,
}

local flash_translation = {
	"Emeralds_Flash1",
	"Emeralds_Flash2",
	"Emeralds_Flash3",
	"Emeralds_Flash3",
	"Emeralds_Flash4",
	"Emeralds_Flash4",
	"Emeralds_Flash4",
	"Emeralds_Flash4",
	"Emeralds_Flash4",
	"Emeralds_Flash3",
	"Emeralds_Flash3",
	"Emeralds_Flash2",
	"Emeralds_Flash1",
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	-- Zeroes are empty
}

hud.add(function(v)
	local p = displayplayer
	if patches_untilized then
		for i = 1, 8 do
			patches[i] = v.cachePatch("CENTCHAOS"..i)
		end
		patches[0] = v.cachePatch("CENTCHAOSX")
		patches[-1] = v.cachePatch("CENTCHAOS8")

		patches_untilized = nil
		draw_scaled = v.drawScaled
		getColormap = v.getColormap
		RandomRange = v.RandomRange
	end

	if multiplayer then
		hud_enable("coopemeralds")
		if mrce.hyperunlocked or p.mrce and p.mrce.hypercheat then
			draw_scaled(bem_x, bem_y, bem_s, patches[-1])
		end
	else
		hud_disable("coopemeralds")
		circle_scale = min(ease_linear(circle_timing, circle_scale, circle_end), FRACUNIT)
		circle_bool = true
	end
end, "scores")

hud.add(function(v, p)
	if circle_scale then

		--Off loading local variables for optimalization purposes
		local leveltime_ang = ANGLE_225 + (emerald_max == emeralds and leveltime*ANG1 or 0)
		local Is_hyper = MRCE_isHyper(p) or false
		local time_half = leveltime>>1
		local time_quad = leveltime>>2

		-- Vanilla Emeralds
		for id,k in ipairs(emerald) do
			-- IF check compares from table whenever or not emerald is in player's possession
			if emeralds & k then
				local pos_ang = id*circle_split + leveltime_ang
				local x = (circle_size * cos(pos_ang)) + em_x
				local y = (circle_size * sin(pos_ang)) + em_y

				local patch = patches[id] or patches[1]
				draw_scaled(x, y,
				circle_scale, patch, 0,
				getColormap(TC_DEFAULT, SKINCOLOR_NONE, flash_translation[(leveltime % #flash_translation) + 1] or nil))

				if Is_hyper and p.mo and p.mo.valid then
					local sine = abs(6*sin((id+time_quad)*black_em_alpha)) >> FRACBITS

					if sine and sine < 10 then
						sine = 8-sine
					end

					local flag = sine << V_ALPHASHIFT

					draw_scaled(x, y,
					circle_scale, patches[0], flag,
					not hyper and getColormap(TC_DEFAULT, hyper_pal[((time_quad+id) % #hyper_pal)+1]))
				end
			end
		end


		-- "Master Emerald"
		if mrce.hyperunlocked or (p.mrce and p.mrce.hypercheat and not p.bot) then
			draw_scaled(em_x, em_y, FixedMul(em_s, circle_scale), patches[8])

			if emeralds == emerald_max then
				if (black_em_timer >= black_em_goal) or (Is_hyper and black_em_timer >= black_em_animt) then
					black_em_timer = 0
					black_em_color = RandomRange(1, 7)
				end

				local flag = 0
				local sine = abs(10*sin(max((Is_hyper == true and leveltime or black_em_timer+black_em_delta), 0)*black_em_alpha)) >> FRACBITS

				if sine and sine < 10 then
					sine = 10-sine
					flag = sine << V_ALPHASHIFT
				end

				if sine then
					draw_scaled(em_x, em_y, FixedMul(em_s, circle_scale), patches[black_em_color], flag)
				end
				black_em_timer = $+1
			end
		end

		if not circle_bool then
			circle_scale = max(ease_linear(circle_timing, circle_scale, circle_start), 0)
		end

		circle_bool = false
	end
end, "game")
