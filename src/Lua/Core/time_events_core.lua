--
--	Mystic Realms Community Editions' OS-Time Events
--	Contributed by Ace Lite, Xian
--

local force_time_event = CV_RegisterVar{
name = "mr_season",
defaultvalue = 0,
flags = CV_NETVAR,
PossibleValue = {MIN = -1, MAX = 128}}

rawset(_G, "time_events", {
	-- Function table for custom maps
	custom_events = {},
	ignore_default = false,
	game_load_date = os.date('*t'),

	-- Description: Adds event into map array.
	-- (int map_id, func function(os_time date))
	--
	-- example usage -> time_events.add_event(1, gfz_srb1_event)
	--
	-- example usage -> time_events.add_event(1, function(date)
	--		if date.mouth then
	--
	--		end
	--end)

	add_event = function(map_id, func)
		if type(func) ~= "function" or not func then
			print("\x85".."Error: ".."\x80While adding event, function argument is either missing, is nil or not a function")
			return
		end

		if type(map_id) ~= "number" then
			print("\x85".."Error: ".."\x80".."While adding event, map_id is not a interger")
			return
		end

		local map = abs(map_id)

		if not time_events.custom_events[map] then
			time_events.custom_events[map] = {}
		end

		table.insert(time_events.custom_events[map], func)
	end,


})

addHook("NetVars", function(net)
	time_events.game_load_date = net($)
end)

local Temp_Key_TX = {}

-- Be free to work on those, I don't care.
-- TO-DO: Add textures (either custom or existing) into presented arrays
-- TO-DO: Add custom events
-- TO-DO: Add into level_headers invidiual time events settings
-- (mapheaderinfo[map].mrce_time_events -> lua.mrce_time_events = TE_SPRING|TE_SUMMER|TE_FALL... etc)

-- Description: This function will swap every texture on linedef and sector. It also swaps all mobjs per provided texture, mobj tables.
-- WARNING: Use this function once!!! Preferably at mapload.
time_events.datasetswapper = function(texture_dataset, mobj_dataset, sky_dataset, skybox_dataset)
	if texture_dataset then
		for sector in sectors.iterate do
			if texture_dataset[sector.floorpic] ~= nil then
				sector.floorpic = texture_dataset[sector.floorpic]
			end
			if texture_dataset[sector.ceilingpic] ~= nil then
				sector.ceilingpic = texture_dataset[sector.ceilingpic]
			end
		end

		--print("sector textures swapped")

		Temp_Key_TX = {}
		for k,v in pairs(texture_dataset) do
			Temp_Key_TX[R_TextureNumForName(k)] = R_TextureNumForName(v)
		end

		for side in sides.iterate do
			if Temp_Key_TX[side.toptexture] then
				side.toptexture = Temp_Key_TX[side.toptexture]
			end

			if Temp_Key_TX[side.midtexture] then
				side.midtexture = Temp_Key_TX[side.midtexture]
			end

			if Temp_Key_TX[side.bottomtexture] then
				side.bottomtexture = Temp_Key_TX[side.bottomtexture]
			end
		end

		--print("wall textures swapped")
	end


	if mobj_dataset then
		for m in mapthings.iterate do
			if m.mobj and m.mobj.valid and mobj_dataset[m.mobj.type] then
				P_SpawnMobjFromMobj(m.mobj, 0, 0, 0, mobj_dataset[m.mobj.type])
				P_RemoveMobj(m.mobj)
			end
		end

		--print("mobjs swapped")
	end

	if sky_dataset and sky_dataset[globallevelskynum] then
		P_SetupLevelSky(sky_dataset[globallevelskynum])
		--print("sky texture swapped")
	end

	if skybox_dataset and skybox_dataset[mapheaderinfo[gamemap].lvlttl] then
		if skybox_dataset[mapheaderinfo[gamemap].lvlttl].mobj and not skybox_dataset[mapheaderinfo[gamemap].lvlttl].remove_skybox then
			P_SetSkyboxMobj(skybox_dataset[mapheaderinfo[gamemap].lvlttl].mobj, skybox_dataset[mapheaderinfo[gamemap].lvlttl].centerpoint or false)
		else
			P_SetSkyboxMobj(nil, nil)
		end
		--print("sky texture swapped")
	end
end

-- Comment: Yes, calculating Easter... WHAT THE !@#^?! -- though this is Orthodox Easter not Catholic Easter...
-- Credit> Thank you someone with nickname Alexandru-Condrin Paranite - https://stackoverflow.com/questions/2192533/function-to-return-date-of-easter-for-the-given-year
local function M_DeterminateEaster(day, mouth, year)
	local a = year % 19
	local b = year / 7
	local c = year % 4
	local d = (19 * a + 16) % 30
	local e = (2 * c + 4 * b + 6 * d) % 7
	local f = d
	local key = f + e + 3
	local easter_mouth, easter_day
	if key > 30 then
		mouth = 5
		day = key-30
	else
		mouth = 4
		day = key
	end

	if easter_mouth == mouth and easter_day == day then
		return true
	else
		return false
	end
end

-- FLAG CONSTANTS FOR MAPHEADER
local TE_SPRING		= 1
local TE_SUMMER		= 2
local TE_FALL		= 4
local TE_WINTER 	= 8

local TE_EASTER		= 16
local TE_APRIL		= 32
local TE_HALLOWEEN	= 64
local TE_CHRISTMAS	= 128

-- ALL FLAGS INCLUDED
local TE_MAX 		= TE_SPRING|TE_SUMMER|TE_FALL|TE_WINTER|TE_EASTER|TE_APRIL|TE_HALLOWEEN|TE_CHRISTMAS

local TE_STRING_ENUM = {
	["TE_SPRING"] 		= TE_SPRING,
	["TE_SUMMER"] 		= TE_SUMMER,
	["TE_FALL"] 		= TE_FALL,
	["TE_WINTER"] 		= TE_WINTER,

	["TE_EASTER"] 		= HO_EASTER,
	["TE_APRIL"] 		= HO_APRIL,
	["TE_HALLOWEEN"] 	= HO_HALLOWEEN,
	["TE_CHRISTMAS"] 	= HO_CHRISTMAS,


	["TE_MAX"]			= TE_MAX,
}

-- I need some function for spliting bit operations seems like,
-- too bad there is no string code executor, or at least calculator :V
local function ST_ToBitsString(str)
	if not str then return 0 end
	local bits = 0

	for bit in str:gmatch("([^|]+)") do
		if TE_STRING_ENUM[bit] then
			bits = $|TE_STRING_ENUM[bit]
		end
	end

	return bits
end

-- Handler hooked function
addHook("MapLoad", function(map)
	local current_date = time_events.game_load_date
	local header = mapheaderinfo[map]
	local settings = ST_ToBitsString(header.mrce_time_events)
	--print(settings)

	--print(mapheaderinfo[gamemap].lvlttl)

	if force_time_event.value == -1 then return end

	local custom_map_events_array = time_events.custom_events[map]
	local global_events_array = time_events.custom_events[0]

	if custom_map_events_array then
		for i = 1, #custom_map_events_array do
			custom_map_events_array[i](current_date, map)
		end
	end

	if global_events_array then
		for i = 1, #global_events_array do
			global_events_array[i](current_date, map)
		end
	end

	if time_events.ignore_default then
		time_events.ignore_default = false
		return
	end

	-- Winter
	if (settings & TE_WINTER and (current_date.month == 12 or current_date.month < 3)) or force_time_event.value == 3 or force_time_event.value == 4 then
		-- Christmas check otherwise pure winter
		if (settings & TE_CHRISTMAS and current_date.month == 12 and current_date.day == 24) or force_time_event.value == 4 then
			time_events.datasetswapper(time_events_data.WINTER_TEXTURES, time_events_data.WINTER_THINGS, time_events_data.WINTER_SKY, time_events_data.WINTER_SKYBOX)
			time_events.datasetswapper(time_events_data.XMAS_TEXTURES, time_events_data.XMAS_THINGS)
		else
			time_events.datasetswapper(time_events_data.WINTER_TEXTURES, time_events_data.WINTER_THINGS, time_events_data.WINTER_SKY, time_events_data.WINTER_SKYBOX)
		end
	-- Halloween
	elseif (settings & TE_HALLOWEEN and (current_date.month == 10 and current_date.day > 23)) or force_time_event.value == 2 then
		time_events.datasetswapper(time_events_data.HALLOWEEN_TEXTURES, time_events_data.HALLOWEEN_THINGS, time_events_data.HALLOWEEN_SKY, time_events_data.HALLOWEEN_SKYBOX)
	-- April Fools
	--elseif settings & HO_APRIL and current_date.month == 4 and current_date.day == 1 then
		time_events.datasetswapper(time_events_data.XMAS_TEXTURES, time_events_data.XMAS_THINGS, time_events_data.HALLOWEEN_SKY)
	-- Orthodox Easter
	--elseif settings & HO_EASTER and current_date.month > 2 and current_date.month < 5 and M_DeterminateEaster(current_date.day, current_date.mouth, current_date.year) then
		time_events.datasetswapper(time_events_data.XMAS_TEXTURES, time_events_data.XMAS_THINGS, time_events_data.HALLOWEEN_SKY)
	end
end)

-- Use for debug, if desired
--[[
hud.add(function(v, p, c)
	local timey = os.date('*t')
	v.drawString(320, 40, "CURRENT DATE", V_YELLOWMAP, "right")
	v.drawString(320, 48, string.format("%02d:%02d:%02d", timey.hour, timey.min, timey.sec), 0, "right")
	v.drawString(320, 56, string.format("%02d/%02d/%04d", timey.day, timey.month, timey.year), 0, "right")

	v.drawString(320, 72, "winter: "+(timey.month == 12), V_YELLOWMAP, "right")
	v.drawString(320, 80, "xmas: "+(timey.month == 12 and timey.day == 24), V_YELLOWMAP, "right")
end, "game")
]]

addHook("MusicChange", function(om, nm)
	if (gamestate ~= GS_LEVEL) then return end
	local current_date = time_events.game_load_date
	local header = mapheaderinfo[gamemap]
	local settings = ST_ToBitsString(header.mrce_time_events) or 0

	if (settings & TE_WINTER and settings & TE_CHRISTMAS and (current_date.month == 12 and current_date.day == 24)) or (force_time_event.value == 3 or force_time_event.value == 4) then
		for m, n in pairs(time_events_data.WINTER_MUSIC) do
			if nm == m then
				if om == n then
					return true
				else
					return n, 0, true, 0
				end
			end
		end
	end
	if (settings & TE_HALLOWEEN and (current_date.month == 10 and current_date.day > 23)) or force_time_event.value == 2 then
		for m, n in pairs(time_events_data.HALLOWEEN_MUSIC) do
			if nm == m then
				if om == n then
					return true
				else
					return n, 0, true, 0
				end
			end
		end
	end
end)