--
--	Mystic Realms Community Editions' OS-Time Events
--	Contributed by Ace Lite, Xian
--

if time_events then
	local funni = false

	-- Primordial Abyss, april fools stuff
	-- Map-ID 0 - Global
	time_events.add_event(0, function(cdate, map)
		if mapheaderinfo[gamemap].lvlttl ~= "Primordial Abyss" then return end
		if (cdate.day ~= 1 and cdate.month ~= 4) and not funni then return end

		for s in sectors.iterate do
			for rover in s.ffloors() do
				if (rover.sector.floorpic == "RAINBOWC"
				or rover.sector.floorpic == "DSBLOCK1"
				or rover.sector.floorpic == "DSBLOCK3") then
					--print("found one")
					if rover.alpha > 5 then
						rover.alpha = 5
						rover.blend = AST_ADD
					end
				end
			end
		end
		time_events.ignore_default = true
	end)

	// should have been event and not it's own script, smh.
	time_events.add_event(784, function(cdate, map)
		local titleSkies = { -- in order of emeralds
			9,     -- blue
			5005,  -- purple
			5004,  -- green
			499,   -- red
			5007,  -- light blue
			5008,  -- yellow
			5009   -- gray
		}

		-- Randomness is surprisingly REALLY fucking consistent,
		-- so we're going to steal a value that changes often.
		-- This way we can (kinda) randomize RNG without even
		-- playing the game. How professional of me.
		-- stjr pls fix
		--P_RandomKey(collectgarbage("count"))

		--xian here, v2.2.12 adds lua access to system time, so we'll use that to seed rng instead. still, gotta say, that was pretty neat usage of an otherwise useless function (in the case of srb2 at least)

		--Skydusk: Well, Doom's RNG is yes consistent for a reason. Demos, literally whole Doom's RNG is just LUT table.
		--So I would have been careful about this, but this usage is fine.


		local st = max((os.time(cdate) % FRACUNIT), 3)
		--print(modifiedgame)
		P_RandomKey(st)

		P_SetupLevelSky(titleSkies[P_RandomKey(#titleSkies)+1], consoleplayer)
		time_events.ignore_default = true
	end)

	--
	--	RANDOM EVENTS FOR NO ONE
	--

	-- just VFZ

	time_events.add_event(107, function(cdate, map)
		if not (P_RandomKey(os.time(cdate) % 8) - 1) then
			P_SetupLevelSky(20000, nil)
		end
	end)

	time_events.add_event(108, function(cdate, map)
		if not (P_RandomKey(os.time(cdate) % 8) - 1) then
			P_SetupLevelSky(20000, nil)
		end
	end)

	time_events.add_event(109, function(cdate, map)
		if not (P_RandomKey(os.time(cdate) % 8) - 1) then
			P_SetupLevelSky(20000, nil)
		end
	end)
end